<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{
    /**
     * Get all Tasks
     */
    public function tasks() {
        
         $tasks = Task::with('tags')->get();
         
         $tasksJson = json_encode($tasks);
         
         return view('tasks.tasks', compact('tasksJson'));
        
    }
    
    /**
     * Get Task
     */
    public function task($id) {
        
        $task = Task::where('id', $id)->with('tags')->first();
        
        if (!$task) {
            return json_encode(['status' => 'error 404']);
        }
         
        $taskJson = json_encode($task);
         
        return view('tasks.task', compact('taskJson'));
    }
    
     /**
     * Create Task
     */
    public function create(Request $request) {
        
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:1000',
            'tags' => 'array',
        ]);
        
        try {
            $task = new Task();
            $task->name = $request->input('name');
            $task->description = $request->input('description'); 
            $task->save();
           
            $task->tags()->attach($request->input('tags'));
        } catch (\Exception $e) {
            return json_encode(['status' => $e]); 
        }
        
        return json_encode(['status' => 'successfully']);
    }
}
