<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{
     /**
     * Get all Tags
     */
    public function tags() {
        
         $tags = Tag::with('tasks')->get();
         
         $tagsJson = json_encode($tags);
         
         return view('tags.tags', compact('tagsJson'));
        
    }
    
    /**
     * Get Tag
     */
    public function tag($id) {
        
        $tag = Tag::where('id', $id)->with('tasks')->first();
        
        if (!$tag) {
            return json_encode(['status' => 'error 404']);
        }
         
        $tagJson = json_encode($tag);
         
        return view('tags.tag', compact('tagJson'));
    }
    
     /**
     * Create Tag
     */
    public function create(Request $request) {
        
        $request->validate([
            'name' => 'required|string|max:255',
        ]);
        
        try {
            $tags = new Tag();
            $tags->name = $request->input('name'); 
            $tags->save();
        } catch (\Exception $e) {
            return json_encode(['status' => $e]); 
        }
        
        return json_encode(['status' => 'successfully']);
    }
}
